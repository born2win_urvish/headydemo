

//
//  CellHomeRanking.swift
//  HeadyDemo
//
//  Created by Urvish Modi on 08/02/18.
//  Copyright © 2018 Urvish Modi. All rights reserved.
//

import UIKit

class CellHomeRanking: UITableViewCell {
    
    @IBOutlet weak var lblProductName: UILabel!
    
    @IBOutlet weak var lblVariants: UILabel!
    
    @IBOutlet weak var lblPriceStartsFrom: UILabel!
    
    @IBOutlet weak var lblViewsCount: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCellFromCategory(productItem:ProductClass) {
        
        let textRange = NSMakeRange(0, "Checkout all \(productItem.variants.count) >>".count)
        let attributedText = NSMutableAttributedString(string: "Checkout all \(productItem.variants.count) >>")
        attributedText.addAttribute(NSAttributedStringKey.underlineStyle , value: NSUnderlineStyle.styleSingle.rawValue, range: textRange)
        
        self.lblProductName.text = productItem.name
        self.lblVariants.attributedText = attributedText
        self.lblPriceStartsFrom.text = "\(productItem.variants[0].price) Rs"
        
        self.lblViewsCount.text = ""
    }
    
    func configureCellFromRanking(tmpRankingObj:RankingSubClass) {
        
        let textRange = NSMakeRange(0, "Checkout all \(tmpRankingObj.product_obj.variants.count) >>".count)
        let attributedText = NSMutableAttributedString(string: "Checkout all \(tmpRankingObj.product_obj.variants.count) >>")
        attributedText.addAttribute(NSAttributedStringKey.underlineStyle , value: NSUnderlineStyle.styleSingle.rawValue, range: textRange)
        
        self.lblProductName.text = tmpRankingObj.product_obj.name
        self.lblVariants.attributedText = attributedText
        self.lblPriceStartsFrom.text = "\(tmpRankingObj.product_obj.variants[0].price) Rs"
        
        if tmpRankingObj.viewCount > 0
        {
            self.lblViewsCount.text = "\(tmpRankingObj.viewCount) Views"
        }
        else
        {
            self.lblViewsCount.text = "Exclusive New launch"
        }
        
    }

}
