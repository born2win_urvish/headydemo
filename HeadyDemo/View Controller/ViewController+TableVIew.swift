
//
//  ViewController+TableVIew.swift
//  HeadyDemo
//
//  Created by Urvish Modi on 07/02/18.
//  Copyright © 2018 Urvish Modi. All rights reserved.
//

import UIKit

extension ViewController:UITableViewDataSource,UITableViewDelegate
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.rankingArr.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let productItems = self.rankingArr[section]
        
        if productItems.products.count > 5
        {
            return 5
        }
        return productItems.products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let productItems = self.rankingArr[indexPath.section]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellHomeRankingSub") as! CellHomeRanking
        
        cell.configureCellFromRanking(tmpRankingObj: productItems.products[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "BuyNowVC") as! BuyNowVC
        controller.productItem = self.rankingArr[indexPath.section].products[indexPath.row].product_obj
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let productItems = self.rankingArr[section]
        
        let headerView = UIView.init(frame: CGRect(x:0,y:0,width:self.view.frame.size.width,height:40))
        
        headerView.backgroundColor = UIColor.groupTableViewBackground
        
        let lblTitle = UILabel.init(frame: CGRect(x:15,y:12.5,width:self.view.frame.size.width-100,height:20))
        lblTitle.text = "\(productItems.name)"
        
        
        let btnSeeAll = UIButton.init(frame: CGRect(x:self.view.frame.size.width-80,y:12.5,width:70,height:20))
        btnSeeAll.setTitle("See All", for: .normal)
        btnSeeAll.setTitleColor(UIColor.darkGray, for: .normal)
        btnSeeAll.titleLabel?.font = UIFont.init(name: (btnSeeAll.titleLabel?.font.fontName)!, size: 16.0)
        
        if productItems.products.count > 5
        {
            btnSeeAll.tag = section
            btnSeeAll.addTarget(self, action: #selector(ViewController.handleSeeAll(sender:)), for: .touchUpInside)
            headerView.addSubview(btnSeeAll)
        }
        
        headerView.addSubview(lblTitle)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    @objc func handleSeeAll(sender:UIButton) {
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "RankingSubVC") as! RankingSubVC
        controller.rankingObj = self.rankingArr[sender.tag]
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
}
