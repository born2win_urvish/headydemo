//
//  ViewController.swift
//  HeadyDemo
//
//  Created by Urvish Modi on 07/02/18.
//  Copyright © 2018 Urvish Modi. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import MBProgressHUD

class ViewController: UIViewController {
    
    
    @IBOutlet weak var tblData: UITableView!
    
    var categoryObjArr = [CategoryClass]()
    var rankingArr = [RankingClass]()
    var allProductsData = [ProductClass]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.getServerData()
        
        self.navigationItem.title = "Heady"
        
        self.navigationController?.navigationBar.tintColor = self.view.tintColor
        
        self.setRightButton()
        
    }
    
    func setRightButton() {
        
        let categoryButton = UIBarButtonItem.init(title: "Categories", style: .plain, target: self, action: #selector(ViewController.openCategoryList))
        self.navigationItem.rightBarButtonItem = categoryButton
    }
    
    @objc func openCategoryList() {
        
        if self.categoryObjArr.count > 0
        {
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "CategoryListVC") as! CategoryListVC
            controller.allCategoryList = self.categoryObjArr
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else
        {
            self.showAlert()
        }
        
        
        
    }
    
    func showLoadingView() {
        let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = "Loading"
    }
    
    func hideLoadingView() {
        
        DispatchQueue.main.async {
            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
        }
    }

    func getServerData() {
        
        self.showLoadingView()
        
        self.categoryObjArr.removeAll()
        self.rankingArr.removeAll()
        self.allProductsData.removeAll()
        self.tblData.isHidden = true
        
        Alamofire.request(
            
            URL(string: "https://stark-spire-93433.herokuapp.com/json")!,
            method: .get,
            parameters: nil)
            .validate()
            .responseJSON { (response) -> Void in
                
                self.hideLoadingView()
                
                guard response.result.isSuccess else {
                    self.showAlert()
                    return
                }
                
                guard let apiResponse = response.result.value else{
                    self.showAlert()
                    return
                }
                
                let jsonData = JSON.init(apiResponse)
                for item in jsonData
                {
                    if item.0 == "categories"
                    {
                        for categoryItem in item.1
                        {
                            let categoryObj = CategoryClass.init()
                            if let category_id = categoryItem.1["id"].int
                            {
                                categoryObj.id = category_id
                            }
                            if let category_name = categoryItem.1["name"].string
                            {
                                categoryObj.name = category_name
                            }
                            
                            var productItemArr = [ProductClass]()
                            for productItem in categoryItem.1["products"].arrayValue
                            {
                                let productItemObj = ProductClass()
                                if let product_id = productItem["id"].int
                                {
                                    productItemObj.id = product_id
                                }
                                if let product_name = productItem["name"].string
                                {
                                    productItemObj.name = product_name
                                }
                                if let product_date = productItem["date_added"].string
                                {
                                    productItemObj.date_added = product_date
                                }
                                
                                var variantsArr = [ProductVariant]()
                                for variantItem in productItem["variants"].arrayValue
                                {
                                    let variantObj = ProductVariant.init()
                                    if let variant_id = variantItem["id"].int
                                    {
                                        variantObj.id = variant_id
                                    }
                                    if let variant_color = variantItem["color"].string
                                    {
                                        variantObj.color = variant_color
                                    }
                                    if let variant_size = variantItem["size"].int
                                    {
                                        variantObj.size = variant_size
                                    }
                                    if let variant_price = variantItem["price"].int
                                    {
                                        variantObj.price = variant_price
                                    }
                                    variantsArr.append(variantObj)
                                }
                                productItemObj.variants = variantsArr.sorted{$0.price < $1.price}
                                
                                if let taxDict = productItem["tax"].dictionary
                                {
                                    if let tax_name = taxDict["name"]?.string
                                    {
                                        productItemObj.taxName = tax_name
                                    }
                                    
                                    if let tax_value = taxDict["value"]?.int
                                    {
                                        productItemObj.taxValue = tax_value
                                    }
                                }
                                productItemArr.append(productItemObj)
                                self.allProductsData.append(productItemObj)
                            }
                            categoryObj.products = productItemArr
                        
                            var childCategoryItemArr = [Int]()
                            for chitCategoryItem in categoryItem.1["child_categories"].arrayValue
                            {
                                if let childCategoryItemId = chitCategoryItem.int
                                {
                                    childCategoryItemArr.append(childCategoryItemId)
                                }
                            }
                            
                            if childCategoryItemArr.count > 0
                            {
                                categoryObj.child_categories = childCategoryItemArr
                                categoryObj.hasChildCategory = true
                            }
                            else
                            {
                                categoryObj.child_categories = [Int]()
                                categoryObj.hasChildCategory = false
                            }
        
                            self.categoryObjArr.append(categoryObj)
                        }
                    }
                    else if item.0 == "rankings"
                    {
                        for rankingItem in item.1
                        {
                            let rankingObj = RankingClass.init()
                            if let ranking_name = rankingItem.1["ranking"].string
                            {
                                rankingObj.name = ranking_name.capitalized
                            }
                            
                            var rankingSubArr = [RankingSubClass]()
                            for ranking_product_item in rankingItem.1["products"].arrayValue
                            {
                                let rankingSubObj = RankingSubClass()
                                if let ranking_sub_id = ranking_product_item["id"].int
                                {
                                    rankingSubObj.product_id = ranking_sub_id
                                }
                                if let ranking_sub_view_count = ranking_product_item["view_count"].int
                                {
                                    rankingSubObj.viewCount = ranking_sub_view_count
                                }
                                rankingSubArr.append(rankingSubObj)
                            }
                            rankingObj.products = rankingSubArr
                            
                            self.rankingArr.append(rankingObj)
                        }
                    }
                }
                
                for product_item_arr in (self.rankingArr.map({$0.products}))
                {
                    for product_item in product_item_arr
                    {
                        product_item.product_obj = self.allProductsData.filter(){$0.id == product_item.product_id}[0]
                    }
                }
                for category_item in (self.categoryObjArr.filter(){$0.child_categories.count > 0})
                {
                    var child_category_objs = [CategoryClass]()
                    for category_id in category_item.child_categories
                    {
                        child_category_objs.append(self.categoryObjArr.filter(){$0.id == category_id}[0])
                    }
                    category_item.child_categories_objs = child_category_objs
                }
                
                DispatchQueue.main.async {
                    self.tblData.isHidden = false
                    self.tblData.reloadData()
                }
        }
    }
    
    func showAlert() {
        let alert = UIAlertController(title: "Alert", message: "Error getting data. Please check your network connection !", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Settings", style: .default, handler: { action in
            
            guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                return
            }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                })
            }
        }))
        
        self.present(alert, animated: true, completion: nil)
    }

}

