

//
//  BuyNowVC.swift
//  HeadyDemo
//
//  Created by Urvish Modi on 08/02/18.
//  Copyright © 2018 Urvish Modi. All rights reserved.
//

import UIKit

class BuyNowVC: UIViewController,UIActionSheetDelegate {
    
    @IBOutlet weak var lblProductName: UILabel!
    
    @IBOutlet weak var lblPrice: UILabel!
    
    @IBOutlet weak var btnSelectSize: UIButton!
    
    @IBOutlet weak var btnSelectColor: UIButton!
    
    
    @IBOutlet weak var lblNumOfItems: UILabel!
    
    
    @IBOutlet weak var lblAmount: UILabel!
    
    @IBOutlet weak var lblTaxCalc: UILabel!
    
    @IBOutlet weak var lblFinalPrice: UILabel!
    
    var productItem = ProductClass.init()
    
    var numOforders = 1
    
    var itemPrice = 0
    var taxRate = 0
    
    var taxAmount = 0
    
    var itemSize = 0
    var itemSizeArr = [Int]()
    var itemColor = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.lblProductName.text = self.productItem.name
        self.itemPrice = self.productItem.variants[0].price
        self.itemSize = self.productItem.variants[0].size
        self.itemColor = self.productItem.variants[0].color
        
        self.taxRate = self.productItem.taxValue
        
        for item_size in self.productItem.variants
        {
            if !(self.itemSizeArr.contains(item_size.size))
            {
                self.itemSizeArr.append(item_size.size)
            }
        }
        
        self.setupView()
    }
    
    func setupView() {
        
        self.lblNumOfItems.text = "\(numOforders)"
        
        if itemSize > 0
        {
            self.btnSelectSize.setTitle("Change: \(itemSize)", for: .normal)
            
            let tmpSizeFilter = self.productItem.variants.filter() { $0.size == itemSize}
            
            itemPrice = tmpSizeFilter[0].price
            
            if itemColor.count > 0
            {
                let tmpColorFilter = self.productItem.variants.filter() { $0.color == itemColor}
                
                itemPrice = tmpColorFilter[0].price
                
                self.btnSelectColor.setTitle("Change: \(itemColor)", for: .normal)
                
            }
            else
            {
                self.btnSelectColor.setTitle("Select Color", for: .normal)
            }
        }
        else
        {
            self.btnSelectSize.setTitle("Select", for: .normal)
            self.btnSelectColor.setTitle("Select", for: .normal)
        }
        
        if taxRate > 0
        {
            taxAmount = ((itemPrice * numOforders) * taxRate)/100
            
            self.lblAmount.text = "\(itemPrice) X \(numOforders) = \((itemPrice * numOforders))"
            self.lblTaxCalc.text = "\(self.productItem.taxName): \(taxRate)%"
            self.lblFinalPrice.text = "Payable Amount: \((itemPrice * numOforders)+taxAmount)"
            self.lblPrice.text = "RS \((itemPrice * numOforders)+taxAmount)"
        }
        else
        {
            self.lblAmount.text = "\(itemPrice) X \(numOforders) = \((itemPrice * numOforders))"
            self.lblTaxCalc.text = ""
            self.lblFinalPrice.text = ""
            self.lblPrice.text = "RS \((itemPrice * numOforders) * numOforders)"
        }
    }
    
    @IBAction func btnSelectSize(_ sender: Any) {
        
        let optionMenu = UIAlertController(title: "Select Size", message: nil, preferredStyle: .actionSheet)
        
        for size_item in itemSizeArr
        {
            let sizeItem = UIAlertAction(title: "\(size_item)", style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                
                self.itemSize = size_item
                
                self.setupView()
                
            })
            
            optionMenu.addAction(sizeItem)
        }
        
        let cancelAction = UIAlertAction.init(title: "Cancel", style: .cancel, handler: {(alert: UIAlertAction!) -> Void in
            
        })
        
        optionMenu.addAction(cancelAction)
       
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    @IBAction func btnSelectColor(_ sender: Any) {
        
        if itemSize > 0
        {
            let optionMenu = UIAlertController(title: "Select Color", message: nil, preferredStyle: .actionSheet)
            
            let tmpSizeFilter = self.productItem.variants.filter() { $0.size == itemSize}
            
            for size_item in tmpSizeFilter
            {
                let sizeItem = UIAlertAction(title: "\(size_item.color)", style: .default, handler: {
                    (alert: UIAlertAction!) -> Void in
                    
                    self.itemColor = size_item.color
                    
                    self.setupView()
                    
                })
                
                optionMenu.addAction(sizeItem)
            }
            
            let cancelAction = UIAlertAction.init(title: "Cancel", style: .cancel, handler: {(alert: UIAlertAction!) -> Void in
                
            })
            
            optionMenu.addAction(cancelAction)
            
            self.present(optionMenu, animated: true, completion: nil)
        }
        else
        {
            let alert = UIAlertController(title: "Alert !", message: "Select size first", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnBuyNow(_ sender: Any) {
        
    }
    
    
    @IBAction func btnAddition(_ sender: Any) {
        
        numOforders = numOforders + 1
        self.setupView()
        
    }
    
    @IBAction func btnMinus(_ sender: Any) {
        
        if numOforders > 1
        {
            numOforders = numOforders - 1
            self.setupView()
        }
        
        
    }
}
