

//
//  RankingSubVC.swift
//  HeadyDemo
//
//  Created by Urvish Modi on 08/02/18.
//  Copyright © 2018 Urvish Modi. All rights reserved.
//

import UIKit

class RankingSubVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tblSubRanking: UITableView!
    
    var rankingObj = RankingClass.init()
    
    var categoryObj = CategoryClass.init()
    
    var fromCategoryClass = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if fromCategoryClass
        {
            self.navigationItem.title = categoryObj.name
        }
        else
        {
            self.navigationItem.title = rankingObj.name
        }
        
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if fromCategoryClass
        {
            return categoryObj.products.count
        }
        else
        {
            return rankingObj.products.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if fromCategoryClass
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellHomeRankingSub") as! CellHomeRanking
            
            cell.configureCellFromCategory(productItem: self.categoryObj.products[indexPath.row])
            
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellHomeRankingSub") as! CellHomeRanking
            
            cell.configureCellFromRanking(tmpRankingObj: self.rankingObj.products[indexPath.row])
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "BuyNowVC") as! BuyNowVC
        
        if fromCategoryClass
        {
            controller.productItem = self.categoryObj.products[indexPath.row]
        }
        else
        {
            controller.productItem = self.rankingObj.products[indexPath.row].product_obj
        }
        
        self.navigationController?.pushViewController(controller, animated: true)
        
    }

}
