
//
//  CategoryClass.swift
//  HeadyDemo
//
//  Created by Urvish Modi on 07/02/18.
//  Copyright © 2018 Urvish Modi. All rights reserved.
//

import UIKit

class CategoryClass: NSObject {
    
    var id = 0
    var name = ""
    var products = [ProductClass]()
    var child_categories = [Int]()
    var child_categories_objs = [CategoryClass]()
    var hasChildCategory = false
    
    override init() {
        super.init()
    }
}
