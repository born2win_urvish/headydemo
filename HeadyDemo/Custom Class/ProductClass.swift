
//
//  ProductClass.swift
//  HeadyDemo
//
//  Created by Urvish Modi on 07/02/18.
//  Copyright © 2018 Urvish Modi. All rights reserved.
//

import UIKit

class ProductClass: NSObject {

    var id = 0
    var name = ""
    var date_added = ""
    var variants = [ProductVariant]()
    var taxName = ""
    var taxValue = 0
    
    override init() {
        super.init()
    }
    
}
