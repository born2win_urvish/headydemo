
//
//  ProductVariant.swift
//  HeadyDemo
//
//  Created by Urvish Modi on 07/02/18.
//  Copyright © 2018 Urvish Modi. All rights reserved.
//

import UIKit

class ProductVariant: NSObject {

    var id = 0
    var color = ""
    var size = 0
    var price = 0
    
    override init() {
        super.init()
    }
}
