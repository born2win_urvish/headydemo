
//
//  CellCategory.swift
//  HeadyDemo
//
//  Created by Urvish Modi on 08/02/18.
//  Copyright © 2018 Urvish Modi. All rights reserved.
//

import UIKit

class CellCategory: UITableViewCell {
    
    @IBOutlet weak var lblCategoryName: UILabel!
    
    @IBOutlet weak var lblNoOfProducts: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
