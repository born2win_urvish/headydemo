

//
//  CategoryListVC.swift
//  HeadyDemo
//
//  Created by Urvish Modi on 08/02/18.
//  Copyright © 2018 Urvish Modi. All rights reserved.
//

import UIKit

class CategoryListVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    
    @IBOutlet weak var tblAllCategories: UITableView!
    
    var categoryList = [CategoryClass]()
    var allCategoryList = [CategoryClass]()
    
    var showChildCategories = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Select Category"
        
        self.setupView()
    }
    
    func setRightButton() {
        
        if showChildCategories
        {
            let categoryButton = UIBarButtonItem.init(title: "All Categories", style: .plain, target: self, action: #selector(handleRightButton))
            self.navigationItem.rightBarButtonItem = categoryButton
        }
        else
        {
            let categoryButton = UIBarButtonItem.init(title: "Child Categories", style: .plain, target: self, action: #selector(handleRightButton))
            self.navigationItem.rightBarButtonItem = categoryButton
        }
    }
    
    @objc func handleRightButton() {
        
        self.showChildCategories = !self.showChildCategories
        self.setupView()
        self.tblAllCategories.reloadData()
    }
    
    func setupView() {
        if showChildCategories
        {
            print(self.allCategoryList.filter(){$0.child_categories_objs.count > 0})
            categoryList = self.allCategoryList.filter(){$0.child_categories_objs.count > 0}
        }
        else
        {
            print(self.allCategoryList.filter(){$0.child_categories_objs.count == 0})
            categoryList = self.allCategoryList.filter(){$0.child_categories_objs.count == 0}
        }
        self.setRightButton()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if showChildCategories
        {
            return self.categoryList.count
        }
        else
        {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if showChildCategories
        {
            return self.categoryList[section].child_categories_objs.count
        }
        else
        {
            return self.categoryList.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if showChildCategories
        {
            let categoryItem = self.categoryList[indexPath.section].child_categories_objs[indexPath.row]
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellCategoryItem") as! CellCategory
            cell.lblCategoryName.text = categoryItem.name
            if categoryItem.products.count > 0
            {
                cell.lblNoOfProducts.text = "\(categoryItem.products.count) Items"
            }
            else
            {
                cell.lblNoOfProducts.text = "\(categoryItem.child_categories.count) Items"
            }
            return cell
        }
        else
        {
            let categoryItem = self.categoryList[indexPath.row]
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellCategoryItem") as! CellCategory
            cell.lblCategoryName.text = categoryItem.name
            if categoryItem.products.count > 0
            {
                cell.lblNoOfProducts.text = "\(categoryItem.products.count) Items"
            }
            else
            {
                cell.lblNoOfProducts.text = "\(categoryItem.child_categories.count) Items"
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if showChildCategories
        {
            
        }
        else
        {
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "RankingSubVC") as! RankingSubVC
            controller.categoryObj = categoryList[indexPath.row]
            controller.fromCategoryClass = true
            self.navigationController?.pushViewController(controller, animated: true)
        }
        
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if showChildCategories
        {
            return self.categoryList[section].name
        }
        else
        {
            return ""
        }
    }

}
